package com.classpath.ordermicroservice.event;

import com.classpath.ordermicroservice.model.EventType;
import com.classpath.ordermicroservice.model.Order;
import lombok.*;
import java.time.LocalDate;

@NoArgsConstructor
@Builder
@AllArgsConstructor
@Setter
@Getter
@ToString
public class OrderEvent {
    private EventType eventType;
    private Order order;
    private String timestamp = LocalDate.now().toString();

}

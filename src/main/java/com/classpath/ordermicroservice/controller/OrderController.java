package com.classpath.ordermicroservice.controller;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @GetMapping
    public Set<Order> fetchAllOrders(){
        return this.orderService.fetchAllOrders();
    }

    @GetMapping("/{id}")
    public Order fetchOrderById(@PathVariable("id") long orderId){
        return this.orderService.findByOrderId(orderId);
    }

    @PostMapping
    public Order saveOrder(@RequestBody Order order){
        return this.orderService.saveOrder(order);
    }

    @DeleteMapping("/{id}")
    public void deleteOrderById(@PathVariable("id") long orderId){
        this.orderService.deleteOrderById(orderId);
    }
}

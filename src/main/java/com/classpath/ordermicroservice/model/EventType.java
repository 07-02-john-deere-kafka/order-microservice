package com.classpath.ordermicroservice.model;

public enum  EventType {
    ORDER_ACCEPTED,
    ORDER_CANCELLED,
    ORDER_DECLINED,
    ORDER_FULFILLED
}

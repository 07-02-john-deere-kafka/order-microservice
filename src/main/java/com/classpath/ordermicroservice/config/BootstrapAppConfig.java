package com.classpath.ordermicroservice.config;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.web.client.RestTemplate;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class BootstrapAppConfig {

    private final OrderRepository orderRepository;
    private final Faker faker = new Faker();

    @EventListener
    public void bootstrapOrders(ApplicationReadyEvent event){
        IntStream.range(0,100).forEach(index -> {
            Order order = Order.builder()
                    .customerName(faker.name().firstName())
                    .date(faker.date().future(4, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                    .price(faker.number().randomDouble(2, 15000, 25000))
                    .email(faker.internet().emailAddress())
                    .build();
            this.orderRepository.save(order);
        });
        log.info("=========Application initialized===========");
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}

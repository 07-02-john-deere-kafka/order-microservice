package com.classpath.ordermicroservice.service;

import com.classpath.ordermicroservice.event.OrderEvent;
import com.classpath.ordermicroservice.model.EventType;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.LongSerializer;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {
    private final OrderRepository orderRepository;
    private final RestTemplate restTemplate;
    private final KafkaTemplate<Long, OrderEvent> kafkaTemplate;

    //update the inventory
    @Transactional
    //@CircuitBreaker(name="inventoryservice", fallbackMethod = "fallback")
    @Retry(name = "inventoryRetry", fallbackMethod = "fallback")
    public Order saveOrder(Order order){
        log.info("Retrying the service call:::::");
        Order savedOrder =  this.orderRepository.save(order);
        ResponseEntity<Integer> responseEntity = this.restTemplate.postForEntity("http://localhost:9222/api/v1/inventory", savedOrder, Integer.class);
        /*communicate with inventory service and upadate the inventory

        Integer response = responseEntity.getBody();
        log.info("Response from inventory microservice :: {}", response);*/
        //post the message to kafka
        OrderEvent orderEvent = OrderEvent.builder().eventType(EventType.ORDER_ACCEPTED).order(savedOrder).timestamp(LocalDate.now().toString()).build();
        ProducerRecord<Long, OrderEvent> producerRecord = new ProducerRecord<>("orders-topic", savedOrder.getId(), orderEvent);
        this.kafkaTemplate.send(producerRecord);
        return savedOrder;
    }

    private Order fallback(Exception exception){
        log.error("Exception while connecting to the microservice :: {}", exception.getMessage());
        return Order.builder().email("dummy@gmail.com").price(10_000).date(LocalDate.now()).customerName("kishore").build();
    }

    public Set<Order> fetchAllOrders(){
        return new HashSet<>(this.orderRepository.findAll());
    }

    public Order findByOrderId(long orderId){
        return this.orderRepository.findById(orderId).orElseThrow(() -> new IllegalArgumentException("Invalid order id"));
    }

    public void deleteOrderById(long orderId){
        this.orderRepository.deleteById(orderId);
    }
}
